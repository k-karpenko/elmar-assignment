package nl.elmar.t4s.domain

case class ShowStats(show: Show,
                    ticketsLeft: Int,
                    ticketsAvailable: Int,
                    status: ShowStatus,
                    price: Double )