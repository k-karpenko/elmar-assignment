package nl.elmar.t4s.domain

case class Purchase( id: Id[Purchase], show: Id[Show], soldTimestamp: Long, price: Double )
