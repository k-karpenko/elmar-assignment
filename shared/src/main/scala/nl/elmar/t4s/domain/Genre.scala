package nl.elmar.t4s.domain

sealed trait Genre
object Genre {
  def byName(genre: String): Genre = {
    genre.toLowerCase match {
      case "musical" ⇒ Musical
      case "comedy" ⇒ Comedy
      case "drama" ⇒ Drama
    }
  }

  case object Musical extends Genre
  case object Comedy extends Genre
  case object Drama extends Genre
}
