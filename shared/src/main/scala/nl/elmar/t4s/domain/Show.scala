package nl.elmar.t4s.domain

case class Show(id: Id[Show], title: String,
                openDate: Long,
                genre: Genre)