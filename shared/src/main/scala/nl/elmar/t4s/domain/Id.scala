package nl.elmar.t4s.domain

import java.util.UUID

case class Id[T]( value: String = UUID.randomUUID().toString )
