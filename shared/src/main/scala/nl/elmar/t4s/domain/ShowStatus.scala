package nl.elmar.t4s.domain

sealed trait ShowStatus

object ShowStatus {
  case object SaleNotStarted extends ShowStatus

  case object OpenForSale extends ShowStatus

  case object SoldOut extends ShowStatus

  case object InThePast extends ShowStatus
}
