package nl.elmar.t4s.cli

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import nl.elmar.t4s.cli.config.Settings
import nl.elmar.t4s.cli.process.{Output, StandardCliProcessor}
import nl.elmar.t4s.cli.remote.T4SServerConnector

import scala.annotation.tailrec
import scala.concurrent.{Await, ExecutionContext}
import scala.concurrent.duration._
import scala.languageFeature.postfixOps

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
object Application {
  implicit val ec = ExecutionContext.Implicits.global

  implicit val actorSystem = ActorSystem("t4sales-cli-system")
  implicit val materializer = ActorMaterializer()

  val settings = new Settings()
  val serverConnector = new T4SServerConnector(settings)
  val processor = new StandardCliProcessor(serverConnector, settings)

  def main( args: Array[String] ): Unit = {
    Console.out.println("=== Tickets For Sale - Terminal ===")
    Console.out.println("===      release 0.1.0820       ===")

    start()
  }

  @tailrec def start(): Unit = {
    Console.out.print(">> ")
    val future = processor.process(Console.in.readLine().trim().split("\\s")) map {
      case Output.Print(message) ⇒
        Console.out.println(message + "\n")
        true
      case Output.Error(message) ⇒
        Console.err.println(s"[ERROR] $message\n")
        true
      case Output.Exit ⇒
        Console.out.println("Goodbye!")
        false
      case Output.Continue ⇒ true
    } recover {
      case e: Throwable ⇒
        Console.err.println(s"[ERROR] ${e.getMessage}\n")
        e.printStackTrace(Console.out)
        true
    }
    Console.out.println()

    val result = Await.result(future, 10 seconds)
    if ( result ) start()
  }

}
