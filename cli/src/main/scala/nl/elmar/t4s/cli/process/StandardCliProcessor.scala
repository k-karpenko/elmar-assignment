package nl.elmar.t4s.cli.process

import java.io.{FileInputStream, FileOutputStream}
import java.text.SimpleDateFormat

import akka.stream.ActorMaterializer
import akka.stream.io.Framing
import akka.stream.scaladsl.{Flow, Source, Sink}
import akka.util.ByteString
import com.github.tototoshi.csv.CSVReader

import nl.elmar.t4s.cli.config.Settings
import nl.elmar.t4s.cli.remote.ServerConnector
import nl.elmar.t4s.domain.{ShowStats, Genre, Id, Show}

import scala.concurrent.{ExecutionContext, Future}

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
object StandardCliProcessor {
  object Operation {
    val query = "query"
    val exit = "exit"
  }
}

class StandardCliProcessor(serverConnector: ServerConnector,
                           settings: Settings)
                          (implicit val materializer: ActorMaterializer) extends CliProcessor {
  import StandardCliProcessor._

  private val format = new SimpleDateFormat("YYYY-MM-dd")

  implicit val ec = ExecutionContext.Implicits.global

  def process( input: Array[String] ): Future[Output] = {
    if ( input.length < 0) Output.Error("No input provided")
    else {
      input(0) match {
        case Operation.query ⇒
          if ( input.length < 4 ) Output.Error("Not enought arguments provided")
          else {
            val future = Source.apply(() ⇒ CSVReader.open("sample.csv").iterator)
              .filter( _.size == 3 )
              .map(l ⇒ Show(Id(), l.head, format.parse(l(1)).getTime, Genre.byName(l(2)) ) )
              .fold(Seq[Show]())(_ :+ _)
              .mapAsync(1) { result ⇒
                serverConnector.queryShows(result.map(_.id), format.parse(input(2)), format.parse(input(3))) map { stats ⇒
                  Output.Print(stats.size.toString)
                }
              }
              .runWith(Sink.head)
            future
          }
        case Operation.exit ⇒ Output.Exit
        case _ ⇒ Output.Error("Unknown command")
      }
    }
  }

}
