package nl.elmar.t4s.cli.process

import scala.concurrent.Future
import scala.language.implicitConversions

sealed trait Output
object Output {
  case object Exit extends Output
  case object Continue extends Output
  case class Print(message: String) extends Output
  case class Error(message: String) extends Output

  implicit def outputToFutureImplicit(output: Output): Future[Output] = Future.successful(output)
}
