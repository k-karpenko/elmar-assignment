package nl.elmar.t4s.cli.config

import com.typesafe.config.{ConfigFactory, Config}

class Settings(config: Config = ConfigFactory.load()) {

  val serverUrl = config.getString("server.url")

  val csvValueSeparator = config.getString("csv.valueSeparator")

}
