package nl.elmar.t4s.cli.process

import scala.concurrent.Future

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
trait CliProcessor {

  def process(input: Array[String]): Future[Output]

}
