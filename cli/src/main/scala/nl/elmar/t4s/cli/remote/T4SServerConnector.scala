package nl.elmar.t4s.cli.remote

import java.util.Date

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpMethods, HttpResponse, HttpRequest}
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, Source, Sink}
import akka.util.Timeout
import nl.elmar.t4s.cli.config.Settings
import nl.elmar.t4s.domain.{Id, Show, ShowStats}

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._

import upickle.default._

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
class T4SServerConnector(settings: Settings)
                        (implicit actorSystem: ActorSystem, actorMaterializer: ActorMaterializer)
  extends ServerConnector {

  implicit val ec = ExecutionContext.Implicits.global
  implicit val timeout = Timeout(5 seconds)

  override def queryShows(showIds:Seq[Id[Show]], queryDate: Date, showDate: Date): Future[Seq[ShowStats]] = {
    Http().singleRequest(
      HttpRequest(uri = "/shows/query", method = HttpMethods.POST,
        entity = "" //write( Map("ids" → showIds, queryDate → queryDate.getTime, showDate → showDate.getTime ) )
      )
    ) flatMap { response ⇒
      response.entity.dataBytes
        .map( b ⇒ read[Seq[ShowStats]](b.decodeString("utf8")) )
        .runFold(Seq[ShowStats]())(_ ++ _)
    }
  }

}
