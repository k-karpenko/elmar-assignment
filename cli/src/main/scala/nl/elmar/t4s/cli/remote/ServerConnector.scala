package nl.elmar.t4s.cli.remote

import java.util.Date

import nl.elmar.t4s.domain.{Id, Show, ShowStats}

import scala.concurrent.Future

/**
 * Ideally, we should re-use Scala.js application code related to the client/server interaction
 * which is already implementing this logic. We just need to hide some aspects behind some fancy abstractions (networking and
 * timeouts implementation)
 *
 * But such implementation is OK for prototype, imho.
 *
 * @author Cyril A. Karpenko
 *
 */
trait ServerConnector {

  def queryShows(ids: Seq[Id[Show]], queryDate: Date, showDate: Date): Future[Seq[ShowStats]]

}
