import moorka.ui.plugin.ResourcesPlugin
import org.scalajs.sbtplugin.ScalaJSPlugin
import org.scalajs.sbtplugin.ScalaJSPlugin.AutoImport._
import sbt._
import sbt.Keys._

scalaVersion := Dependencies.Versions.scala

val commonSettings = Seq(
  version := "0.1.0-SNAPSHOT",
  organization := "nl.elmar.t4s",
  resolvers += Resolver.url("Moorka", url("http://dl.bintray.com/tenderowls/moorka"))(Resolver.ivyStylePatterns),
  resolvers += "Flexis Thirdparty Snapshots" at "https://nexus.flexis.ru/content/repositories/thirdparty-snapshots",
  resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots",
  scalaVersion := Dependencies.Versions.scala,
  scalacOptions ++= Seq("-deprecation", "-feature", "-Xfatal-warnings", "-language:postfixOps")
)

lazy val cli = project.
  settings(commonSettings: _*).
  settings(
    libraryDependencies ++= Dependencies.cli.value
  ).
  dependsOn(sharedJVM)


lazy val client = project.
  enablePlugins(ResourcesPlugin).
  enablePlugins(ScalaJSPlugin).
  settings(commonSettings: _*).
  settings(
    libraryDependencies ++= Dependencies.client.value
  ).
  dependsOn(sharedJS)

lazy val server = project.
  settings(name := "app-backend").
  settings(commonSettings:_*).
  settings(
    libraryDependencies ++= Dependencies.server.value
  ).
  dependsOn(sharedJVM)

lazy val shared = crossProject.crossType(CrossType.Pure).
  settings(commonSettings: _*).
  settings(
    libraryDependencies ++= Dependencies.shared.value
  )

lazy val sharedJS = shared.js
lazy val sharedJVM = shared.jvm

lazy val root = (project in file(".")).
  aggregate(
    server, client,
    sharedJS, sharedJVM
  )
