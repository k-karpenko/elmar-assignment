package nl.elmar.t4s.server.service

import akka.actor.{ActorSystem, ActorRef}
import akka.stream.ActorMaterializer
import akka.pattern.ask
import akka.util.Timeout

import com.typesafe.scalalogging.slf4j.LazyLogging

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

import nl.elmar.t4s.domain.{Show, Id}
import nl.elmar.t4s.server.actors.TicketsFrontendActor

import upickle.default._

import akka.http.scaladsl.server.Directives._

trait TicketService extends CorsSupport with LazyLogging {

  implicit val actorSystem: ActorSystem
  implicit val actorMaterializer: ActorMaterializer

  implicit val timeout = Timeout(300 millis)
  implicit val ec = ExecutionContext.Implicits.global

  protected val ticketsFrontendActor: ActorRef

  protected lazy val ticketRoutes = purchaseRoute

  private lazy val purchaseRoute = {
    respondWithHeaders(corsHeaders) {
      path("ticket") {
        (post & entity(as[String]) ) { json =>
          val (showId, date) = read[(Id[Show], Long)](json)

          val purchaseFuture = ticketsFrontendActor ? TicketsFrontendActor.Request.Purchase(showId, date)
          complete(
            purchaseFuture map {
              case TicketsFrontendActor.Response.Sold(result) ⇒
                logger.info("Purchase has been processed successfully!")
                ""
              case _ ⇒ throw new IllegalStateException("request failed")
            }
          )
        }
      }
    }
  }

}
