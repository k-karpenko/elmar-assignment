package nl.elmar.t4s.server.actors

import java.nio.file.{Files, Paths}
import java.text.SimpleDateFormat

import akka.actor.Props
import akka.persistence.{RecoveryCompleted, SnapshotOffer, PersistentActor}
import com.typesafe.scalalogging.slf4j.LazyLogging
import nl.elmar.t4s.domain.{Genre, Id, Show}
import nl.elmar.t4s.server.config.Settings

import scala.collection.JavaConversions._

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
object ShowsBackendActor {

  def props(config: Settings) = Props(classOf[ShowsBackendActor], config)

  sealed trait Request
  object Request {
    case object RequestAll extends Request
    case class ResolveOne(id: Id[Show]) extends Request
  }

  sealed trait Response
  object Response {
    case class Records(records: Seq[Show]) extends Response
    case class Record(record: Option[Show]) extends Response
  }

}

class ShowsBackendActor(config: Settings) extends PersistentActor with LazyLogging {
  import ShowsBackendActor._

  var records = loadInitialData()

  override def receiveRecover: Receive = {
    case SnapshotOffer(_, show: Show) ⇒
      records = records :+ show
    case RecoveryCompleted ⇒ logger.info("Shows list has been recovered")
  }

  override def receiveCommand: Receive = {
    case Request.RequestAll ⇒ sender() ! Response.Records(records)
    case Request.ResolveOne(id) ⇒ sender() ! Response.Record(records.find(_.id == id))
    case e: Any ⇒ logger.info(e.toString)
  }

  // TODO: should be removed as an initial test stuff
  private def loadInitialData(): Seq[Show] = {
    if ( !config.InitialData.initialDataLoadEnabled(config.InitialData.showsType) ) Seq.empty
    else {
      val initialDataPath = Paths.get(config.InitialData.initialDataPath( config.InitialData.showsType))
      val records = Files.readAllLines(initialDataPath) map { line ⇒
        val parts = line.split(";")
        val (title, openDate, genre) = (parts(0), parts(1), parts(3))
        val dateFormat = new SimpleDateFormat(config.InitialData.initialDataDateFormat(config.InitialData.showsType))

        Show( id = Id(), title = title,
          openDate = dateFormat.parse(openDate).getTime,
          genre = Genre.byName(genre)
        )
      }

      records.toSeq
    }
  }

  override def persistenceId: String = "shows-backend-actor"
}
