package nl.elmar.t4s.server

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer

import scala.concurrent.ExecutionContext

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
object Main extends App {
  implicit val actorSystem = ActorSystem("tickets4sale-system")
  implicit val actorMaterializer = ActorMaterializer()
  implicit val ec = ExecutionContext.Implicits.global

  val server = new AppServer()
  server.start()
}
