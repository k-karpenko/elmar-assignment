package nl.elmar.t4s.server.service

import akka.actor.ActorRef
import akka.http.scaladsl.marshalling.ToResponseMarshallable
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.PathMatchers.LongNumber
import akka.pattern.ask
import akka.util.Timeout
import scala.languageFeature.postfixOps
import scala.concurrent.duration._

import upickle.default._

import nl.elmar.t4s.server.actors.ShowsFrontendActor

trait ShowService extends CorsSupport {

  implicit private val ec = scala.concurrent.ExecutionContext.Implicits.global
  implicit private val timeout = Timeout(200 millis)

  protected val showsFrontendActor: ActorRef

  protected lazy val showRoutes = queryRoute

  private lazy val queryRoute = {
    respondWithHeaders(corsHeaders) (
      pathPrefix("show" ) {
        get {
          path(LongNumber) { (forDate) ⇒
            complete {
              val responseFuture = showsFrontendActor ? ShowsFrontendActor.Request.Filter(forDate) map {
                case r: ShowsFrontendActor.Response.Stats ⇒ write(r.data)
                case _ ⇒ write("error" → "failure")
              } recover {
                case e: Throwable ⇒ write("error" → e.getMessage)
              } map { r ⇒ ToResponseMarshallable(r) }

              responseFuture.mapTo[ToResponseMarshallable]
            }
          }
        }
      } ~
      pathPrefix("list" ) {
        get {
          complete {
            val responseFuture = showsFrontendActor ? ShowsFrontendActor.Request.All map {
              case r: ShowsFrontendActor.Response.Records ⇒ write(r.data)
              case _ ⇒ write("error" → "failure")
            } recover {
              case e: Throwable ⇒ write("error" → e.getMessage)
            } map { r ⇒ ToResponseMarshallable(r) }

            responseFuture.mapTo[ToResponseMarshallable]
          }
        }
      }
    )
  }

}
