package nl.elmar.t4s.server.actors

import java.util.concurrent.TimeUnit

import akka.actor.{Props, ActorRef, Actor}

import akka.pattern.{ask, pipe}
import akka.util.Timeout
import scala.concurrent.duration._

import nl.elmar.t4s.domain.{Purchase, Genre, Show, Id}
import nl.elmar.t4s.server.config.Settings


import scala.concurrent.Future

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
object FinancialDataActor {

  def props(settings: Settings) = Props(classOf[Settings], settings)

  sealed trait Request
  object Request {
    case class CalculatePrice(timestamp: Long, showId: Id[Show]) extends Request

    case class CalculateTicketsAvailable(timestamp: Long, showId: Id[Show]) extends Request

    case class CalculateTicketsLeft(timestamp: Long, showId: Id[Show]) extends Request

    case class CalculateDaysUntilClosing(timestamp: Long, showId: Id[Show]) extends Request
  }

  sealed trait Response
  object Response {

    case class CalculationResult[T](value: T) extends Response

  }

}

class FinancialDataActor(config: Settings, showBackendActor: ActorRef, ticketsBackendActor: ActorRef) extends Actor {
  import FinancialDataActor._

  implicit val ec = context.system.dispatcher
  implicit val timeout = Timeout(300 millis)

  override def receive: Receive = {
    case Request.CalculateDaysUntilClosing(ts, showId) ⇒
      daysUntilClosing(ts, showId) map (r ⇒ Response.CalculationResult(r)) pipeTo sender()

    case Request.CalculatePrice(ts, showId) ⇒
      calculatePrice(ts, showId) map (r ⇒ Response.CalculationResult(r)) pipeTo sender()

    case Request.CalculateTicketsAvailable(ts, showId) ⇒
      calculatePrice(ts, showId) map (r ⇒ Response.CalculationResult(r)) pipeTo sender()

    case Request.CalculateTicketsLeft(ts, showId) ⇒
      calculatePrice(ts, showId) map (r ⇒ Response.CalculationResult(r)) pipeTo sender()
  }

  private def calculatePrice(dateTimestamp: Long, showId: Id[Show]): Future[Double] = {
    resolveShow(showId) map {
      case Some(show) ⇒
        val price = BigDecimal(
          show.genre match {
            case Genre.Comedy ⇒ config.Price.comedyPrice
            case Genre.Drama ⇒ config.Price.dramaPrice
            case Genre.Musical ⇒ config.Price.musicalPrice
          }
        )

        val finalPrice =
          if ( daysUntilClosing(dateTimestamp, show) > config.Sales.secondQuartileDays) {
            price * config.Sales.closingDiscount
          } else {
            price
          }

        finalPrice.doubleValue()
      case None ⇒ throw new IllegalStateException("unknown show")
    }
  }

  private def calculateAvailable(dateTimestamp: Long, showId: Id[Show]): Future[Int] = {
    resolveShow(showId) flatMap {
      case Some(show) ⇒
        val daysLeft = daysUntilClosing(dateTimestamp, show)

        val total = daysLeft match {
          case x if x > config.Sales.firstQuartileDays && x < config.Sales.secondQuartileDays ⇒
            config.Sales.Limits.salesAtBigHall
          case _ ⇒ config.Sales.Limits.salesAtSmallHall
        }

        calculateLeft(dateTimestamp, show) map { left ⇒
          total - left
        }
      case None ⇒ throw new IllegalStateException("unknown show")
    }
  }

  private def calculateLeft(dateTimestamp: Long, show: Show): Future[Int] = {
    resolvePurchases() map { purchases ⇒
      purchases.count(o ⇒ o.show == show.id || sameDay(o.soldTimestamp, dateTimestamp))
    }
  }

  private def daysUntilClosing(dateTimestamp: Long, showId: Id[Show]): Future[Int] = {
    resolveShow(showId) map {
      case Some(show) ⇒ daysUntilClosing(dateTimestamp, show)
      case None ⇒ throw new IllegalStateException("unknown show")
    }
  }

  private def daysUntilClosing(dateTimestamp: Long, show: Show): Int = {
    val days = config.Sales.RunningTime - TimeUnit.MILLISECONDS.toDays(show.openDate - dateTimestamp)
    days.toInt
  }

  private def sameDay(first: Long, second: Long): Boolean = {
    Math.abs(second-first) < TimeUnit.HOURS.toMillis(24)
  }

  private def resolvePurchases(): Future[Seq[Purchase]] = {
    val resolvingFuture = ticketsBackendActor ? TicketsBackendActor.Request.RequestAll

    resolvingFuture map {
      case TicketsBackendActor.Response.Records(records) ⇒ records
    }
  }

  private def resolveShow(id: Id[Show]): Future[Option[Show]] = {
    val resolvingFuture = showBackendActor ? ShowsBackendActor.Request.ResolveOne(id)

    resolvingFuture map {
      case ShowsBackendActor.Response.Record(record) ⇒ record
    }
  }
}
