package nl.elmar.t4s.server.service

import akka.http.scaladsl.model.HttpMethods._
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.model.headers._
import akka.http.scaladsl.server.{Directive0, Route}
import akka.http.scaladsl.server.Directives._
import com.typesafe.config.ConfigFactory

trait CorsSupport {
  val optionsSupport = {
    options { complete("") }
  }

  val corsHeaders = List(RawHeader("Access-Control-Allow-Origin", "*"),
          RawHeader("Access-Control-Allow-Methods", "GET, POST, PUT, OPTIONS, DELETE"),
          RawHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Access-Control-Allow-Origin, Content-Type, Accept, Authorization"))
}
