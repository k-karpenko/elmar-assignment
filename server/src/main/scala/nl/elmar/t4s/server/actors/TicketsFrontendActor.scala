package nl.elmar.t4s.server.actors

import akka.actor.{ActorRef, Actor, Props}
import akka.util.Timeout
import com.typesafe.scalalogging.slf4j.LazyLogging
import nl.elmar.t4s.domain._
import nl.elmar.t4s.server.config.Settings

import akka.pattern.{ask, pipe}

import scala.concurrent.Future
import scala.concurrent.duration._


/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
object TicketsFrontendActor {

  def props(showsBackendActor: ActorRef,
            ticketsBackendActor: ActorRef,
            financialDataActor: ActorRef,
            settings: Settings): Props = {
    Props(classOf[TicketsFrontendActor], showsBackendActor, ticketsBackendActor, financialDataActor, settings)
  }

  case class PurchasesList(records: Seq[Purchase])

  private[TicketsFrontendActor] case class FinancialDetails(ticketsLeft: Int, ticketsAvailable: Int,
                                                            price: Double)

  sealed trait Request
  object Request {
    case class Purchase(show: Id[Show], dateTimestamp: Long) extends Request
    case class GenerateReport(dateTimestamp: Long) extends Request
  }

  sealed trait Response
  object Response {
    case class Sales(result: Seq[ShowStats]) extends Response
    case class Sold(result: Purchase) extends Response
  }

  sealed trait Failure
  object Failure {
    case object NoTicketsLeft extends Failure
    case object SalesClosed extends Failure
  }
}

class TicketsFrontendActor(showsBackendActor: ActorRef,
                           ticketsBackendActor: ActorRef,
                           financialDataActor: ActorRef,
                           config: Settings) extends Actor with LazyLogging {
  import TicketsFrontendActor._

  implicit val ec = context.system.dispatcher
  implicit val timeout = Timeout(300 millis)

  override def receive: Receive = {
    case Request.Purchase(show, purchaseDate) ⇒
      val finalResult =
        resolveFinancialDetails(show, purchaseDate) map {details ⇒
          if ( details.ticketsAvailable == 0 ) {
            TicketsFrontendActor.Failure.NoTicketsLeft
          } else {
            val operation = Purchase(Id(), show, System.currentTimeMillis(), details.price)
            TicketsFrontendActor.Response.Sold(operation)
          }
        }

      finalResult pipeTo sender()

    case Request.GenerateReport(dateTimestamp) ⇒
      val resultsFuture = showsBackendActor ? ShowsBackendActor.Request.RequestAll

      resultsFuture map {
        case ShowsBackendActor.Response.Records(records) ⇒
          val futures = records map { record ⇒
            resolveFinancialDetails(showId = record.id, dateTimestamp) map { details ⇒
              ShowStats(
                status = ShowStatus.InThePast,
                show = record,
                ticketsLeft = details.ticketsLeft,
                ticketsAvailable = details.ticketsAvailable,
                price = details.price
              )
            }
          }

          Future.sequence(futures) map ( seq ⇒ TicketsFrontendActor.Response.Sales(seq) )
      } pipeTo sender()
  }

  private def resolveFinancialDetails( showId: Id[Show], dateTimestamp: Long): Future[FinancialDetails] = {
    val availableFuture = financialDataActor ?
      FinancialDataActor.Request.CalculateTicketsAvailable(dateTimestamp, showId)

    val leftFuture = financialDataActor ?
      FinancialDataActor.Request.CalculateTicketsLeft(dateTimestamp, showId)

    val priceFuture = financialDataActor ?
      FinancialDataActor.Request.CalculatePrice(dateTimestamp, showId)

    for {
      ticketsLeft ← leftFuture.mapTo[FinancialDataActor.Response.CalculationResult[Int]]
      ticketsAvailable ← availableFuture.mapTo[FinancialDataActor.Response.CalculationResult[Int]]
      currentPrice ← priceFuture.mapTo[FinancialDataActor.Response.CalculationResult[Double]]
    } yield FinancialDetails(ticketsLeft.value, ticketsAvailable.value, currentPrice.value)
  }

}
