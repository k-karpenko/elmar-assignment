package nl.elmar.t4s.server

import akka.actor.{ActorRef, ActorSystem}
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Sink
import com.typesafe.scalalogging.slf4j.LazyLogging
import nl.elmar.t4s.server.actors.{RootActors, TicketsFrontendActor, TicketsFrontendActor$, ShowsFrontendActor}
import nl.elmar.t4s.server.config.Settings
import nl.elmar.t4s.server.service.Services

import scala.concurrent.ExecutionContext

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
class AppServer(implicit val system: ActorSystem,
                val actorMaterializer: ActorMaterializer) extends Services with RootActors
  with LazyLogging {

  override val actorSystem = system
  override val settings = new Settings()

  def start()(implicit ec: ExecutionContext): Unit = {
    val serverBinding = Http().bind(interface = settings.serverHost, port = settings.serverPort)

    serverBinding.to(Sink.foreach {
      connection =>
        println("Accepted new connection from: " + connection.remoteAddress)
        connection handleWith Route.handlerFlow {
          buildRoute()
        }
    }).run()

    logger.info(s"Server started on ${settings.serverHost}:${settings.serverPort}")
  }

}
