package nl.elmar.t4s.server.actors

import akka.actor.{Actor, ActorRef, Props}
import akka.pattern.{ask, pipe}
import akka.util.Timeout

import scala.concurrent.duration._
import scala.languageFeature.postfixOps

import com.typesafe.scalalogging.slf4j.LazyLogging

import nl.elmar.t4s.domain._
import nl.elmar.t4s.server.config.Settings

object ShowsFrontendActor {

  def props(ticketsActor: ActorRef, config: Settings): Props = Props(classOf[ShowsFrontendActor], ticketsActor, config)

  sealed trait Request
  object Request {
    case class Filter(dateTimestamp: Long) extends Request
    case object All extends Request
  }

  sealed trait Response
  object Response {
    case class Records(data: Seq[Show]) extends Response
    case class Stats(data: Seq[ShowStats]) extends Response
  }

}

class ShowsFrontendActor(ticketsFrontendActor: ActorRef,
                         showsBackendActor: ActorRef,
                         config: Settings) extends Actor with LazyLogging {
  import ShowsFrontendActor._

  implicit val timeout = Timeout( 5 seconds )
  implicit val ec = context.system.dispatcher

  override def receive: Receive = {
    case Request.Filter(startDate) ⇒
      val salesFuture = ticketsFrontendActor ? TicketsFrontendActor.Request.GenerateReport(startDate)

      val originalSender = sender()
      salesFuture.mapTo[TicketsFrontendActor.Response.Sales] map { result ⇒
        originalSender ! Response.Stats(result.result)
      }

    case Request.All ⇒
      val requestFuture = showsBackendActor ? ShowsBackendActor.Request.RequestAll
      val originalSender = sender()
      requestFuture.mapTo[ShowsBackendActor.Response.Records].map(r ⇒ Response.Records(r.records)) pipeTo originalSender
  }

}
