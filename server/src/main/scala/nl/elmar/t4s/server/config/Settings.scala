package nl.elmar.t4s.server.config

import com.typesafe.config.{Config, ConfigFactory}

import scala.concurrent.duration.Duration

class Settings(val config: Config = ConfigFactory.load() ) {

  object InitialData {
    val showsType = "shows"
    val purchasesType = "shows"

    def initialDataDateFormat(tpe: String) = config.getString(s"services.$tpe.initialDataDateFormat")
    def initialDataPath(tpe: String) = config.getString(s"services.$tpe.initialDataLoadPath")
    def initialDataLoadEnabled(tpe: String) = config.getBoolean(s"services.$tpe.initialDataLoadEnabled")
  }

  object Sales {
    lazy val RunningTime = config.getInt("financial.sales.runningTime")

    lazy val firstQuartileDays = config.getInt("financial.sales.firstQuartileLength")
    lazy val secondQuartileDays = config.getInt("financial.sales.secondQuartileLength")

    lazy val openBefore = config.getInt("financial.sales.openBefore")
    lazy val soldOutBefore = config.getInt("financial.sales.soldOutBefore")
    lazy val closingDiscount = config.getDouble("financial.sales.closingDiscount")

    object Limits {
      lazy val salesAtBigHall = config.getInt("financial.sales.limits.sales.big-hall")
      lazy val salesAtSmallHall = config.getInt("financial.sales.limits.sales.small-hall")
      lazy val attendanceAtBigHall = config.getInt("financial.sales.limits.attendance.big-hall")
      lazy val attendanceAtSmallHall = config.getInt("financial.sales.limits.attendance.small-hall")
    }

  }

  object Price {
    lazy val comedyPrice = config.getDouble("financial.prices.comedy")
    lazy val dramaPrice = config.getDouble("financial.prices.drama")
    lazy val musicalPrice = config.getDouble("financial.prices.musical")
  }

  lazy val serverHost = config.getString("server.host")
  lazy val serverPort = config.getInt("server.port")


}
