package nl.elmar.t4s.server.actors

import java.nio.file.{Paths, Files}
import java.text.SimpleDateFormat

import akka.actor.Props
import akka.persistence.{RecoveryCompleted, SnapshotOffer, PersistentActor}
import com.typesafe.scalalogging.slf4j.LazyLogging
import nl.elmar.t4s.domain.{Show, Id, Purchase}
import nl.elmar.t4s.server.actors.TicketsFrontendActor.PurchasesList
import nl.elmar.t4s.server.config.Settings

import scala.collection.JavaConversions._

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
object TicketsBackendActor {
  def props(config: Settings) = Props(classOf[TicketsBackendActor], config)

  sealed trait Request
  object Request {
    case object RequestAll extends Request
    case class RequestSingle(id: Id[Show]) extends Request
  }

  sealed trait Response
  object Response {
    case class Records(tickets: Seq[Purchase]) extends Response
  }
}

class TicketsBackendActor(config: Settings) extends PersistentActor with LazyLogging {
  import TicketsBackendActor._

  var operations = Seq[Purchase]()

  override def receiveRecover: Receive = {
    case SnapshotOffer(_, PurchasesList(snapshot)) ⇒ operations = snapshot
    case RecoveryCompleted ⇒ logger.info("Purchases list recovered")
  }

  override def receiveCommand: Receive = {
    case Request.RequestAll ⇒
    case Request.RequestSingle(id) ⇒
  }

  override def persistenceId: String = "tickets-backend-actor"
}
