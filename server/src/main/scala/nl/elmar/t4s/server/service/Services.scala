package nl.elmar.t4s.server.service

import akka.actor.ActorSystem
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._

trait Services extends ShowService with TicketService {

  def buildRoute()(implicit system: ActorSystem): Route = showRoutes ~ ticketRoutes

}
