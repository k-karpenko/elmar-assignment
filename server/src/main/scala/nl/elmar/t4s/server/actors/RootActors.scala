package nl.elmar.t4s.server.actors

import akka.actor.{ActorSystem, ActorRef}
import nl.elmar.t4s.server.config.Settings

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
trait RootActors {

  val actorSystem: ActorSystem
  val settings: Settings

  lazy val showsBackendActor: ActorRef = actorSystem.actorOf(ShowsBackendActor.props(settings))

  lazy val financialActor: ActorRef = actorSystem.actorOf(FinancialDataActor.props(settings))

  lazy val ticketsBackendActor: ActorRef = actorSystem.actorOf(TicketsBackendActor.props(settings))
  lazy val ticketsFrontendActor: ActorRef = actorSystem.actorOf(TicketsFrontendActor.props(showsBackendActor,
    ticketsBackendActor, financialActor, settings))

  lazy val showsFrontendActor: ActorRef = actorSystem.actorOf(
    ShowsFrontendActor.props(ticketsBackendActor, settings))


}
