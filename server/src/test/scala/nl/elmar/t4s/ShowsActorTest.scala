package nl.elmar.t4s

import akka.actor.ActorSystem
import akka.testkit._
import com.typesafe.config.{ConfigValueFactory, ConfigFactory, Config}
import nl.elmar.t4s.server.actors.ShowsBackendActor
import nl.elmar.t4s.server.config.Settings
import org.scalatest.{BeforeAndAfterAll, WordSpecLike}
import scala.concurrent.duration._

class ShowsActorTest extends TestKit(ActorSystem("shows-actor-test")) with WordSpecLike with BeforeAndAfterAll with ImplicitSender {
  "ShowBackendActor" when {
    "start" should {
      "not load initial database state if this option has been disabled in application configuration" in new ShowBackendActorTest(ConfigFactory.load()
        .withValue("services.shows.initialDataLoadEnabled", ConfigValueFactory.fromAnyRef("false"))) {
        assertResult(Seq.empty) {
          actorRef.underlyingActor.records
        }
      }

      "load initial database state on actor start" in new ShowBackendActorTest {
        assertResult(2) {
          actorRef.underlyingActor.records.size
        }
      }
    }

    "receives Request.RequestAll" should {
      "response with all records which are currently in the storage" in new ShowBackendActorTest {
        testActor ! ShowsBackendActor.Request.RequestAll
        fishForMessage(300.millis) {
          case ShowsBackendActor.Response.Records(records) ⇒
            assertResult(2)(records.size)
            false
          case _ ⇒ true
        }
      }
    }

    "receives Request.ResolveOne" should {
      "return requested record in a case it has been found in storage" in new ShowBackendActorTest {
        testActor ! ShowsBackendActor.Request.ResolveOne(actorRef.underlyingActor.records.head.id)
        fishForMessage(300.millis) {
          case ShowsBackendActor.Response.Record(Some(record)) ⇒
            assertResult(record.id)(actorRef.underlyingActor.records.head.id)
            false
          case _ ⇒ true
        }
      }

      "return None in a case it hasn't been found in storage" in new ShowBackendActorTest {
        testActor ! ShowsBackendActor.Request.ResolveOne(actorRef.underlyingActor.records.head.id)
        fishForMessage(300.millis) {
          case ShowsBackendActor.Response.Record(None) ⇒ false
          case _ ⇒ true
        }
      }
    }
  }

  class ShowBackendActorTest(config: Config = ConfigFactory.load()) {
    val actorRef = TestActorRef[ShowsBackendActor](ShowsBackendActor.props(new Settings(config)))
  }

}
