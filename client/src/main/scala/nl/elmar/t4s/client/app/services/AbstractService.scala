package nl.elmar.t4s.client.app.services

import java.util.concurrent.TimeoutException

import scala.concurrent.{Promise, Future}
import scala.concurrent.duration.FiniteDuration
import scala.util.Failure

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
trait AbstractService {

  def failAfter[T](timeout: FiniteDuration): Future[T] = {
    val tick = Promise[T]()
    scala.scalajs.js.timers.setTimeout(timeout)(tick.complete(Failure(new TimeoutException())))
    tick.future
  }

}
