package nl.elmar.t4s.client.app

import moorka.rx.{Val, Var}
import nl.elmar.t4s.client.app.views.{PurchaseView, ShowsListView, NavigationView}
import nl.elmar.t4s.client.system.T4SaleSystem
import slogging.LazyLogging
import vaska.JSObj
import felix._

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
@scala.scalajs.js.annotation.JSExport
object Main extends LazyLogging {

  @scala.scalajs.js.annotation.JSExport
  def main(jsa: vaska.JSAccess): Unit = {
    implicit val system = new T4SaleSystem(jsa)
    implicit val ec = system.ec

    val showsListPage = new ShowsListView
    val purchasePage = new PurchaseView

    val currentView = Var[Component](showsListPage)

    val navigationView = new NavigationView(
      onListView = () ⇒ {
        currentView pull Val(showsListPage)
      },
      onPurchaseView = () ⇒ {
        currentView pull Val(purchasePage)
      }
    )

    val el = 'div(
      navigationView,
      currentView
    )

    system.document.getAndSaveAs("body", "startupBody") foreach { body ⇒
      jsa.request[Unit]("init") foreach { _ ⇒
        body.call[JSObj]("appendChild", el.ref) foreach { _ ⇒
          body.free()
        }
      }
    }
  }

}
