package nl.elmar.t4s.client.app.views

import felix.core.FelixSystem
import felix.vdom.{Element, Component}

import felix._
import nl.elmar.t4s.client.system.PageManager
import slogging.LazyLogging

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
class NavigationView(onListView: () ⇒ Unit, onPurchaseView: () ⇒ Unit)
                    (implicit val system: FelixSystem) extends Component with LazyLogging {

  override def start: Element = {
    'div( 'class := "nav",
      'button(
        "Shows list",
        'click listen {
          logger.info("Shows list?")
          onListView()
        }
      ),
      'button(
        "Purchase",
        'click listen {
          logger.info("Purchase view")
          onPurchaseView()
        }
      )
    )
  }

}
