package nl.elmar.t4s.client.system

import felix._
import nl.elmar.t4s.client.app.services.{TicketServiceImpl, ShowServiceImpl}
import nl.elmar.t4s.client.system.utils.Logging
import vaska.JSAccess

import scala.concurrent.ExecutionContext

object T4SaleSystem {

  final class Inject(implicit ec: ExecutionContext) {
    val showService = new ShowServiceImpl
    val ticketService = new TicketServiceImpl

    val pageManager = new PageManager()

    val serverUrl = "http://localhost:9190/"
  }

}

class T4SaleSystem(jsa: JSAccess) extends FelixSystem {
  import T4SaleSystem._

  val jsAccess = jsa
  implicit val ec = scala.scalajs.concurrent.JSExecutionContext.Implicits.runNow

  Logging.init(this)
  val inject = new Inject()
}