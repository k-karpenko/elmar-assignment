package nl.elmar.t4s.client

import felix._

/**
 * @author Cyril A. Karpenko <self@nikelin.ru>
 */
package object utils {
  object EmptyComponent {
    def apply()(implicit system: FelixSystem) = new EmptyComponent().start
  }

  class EmptyComponent()(implicit val system: FelixSystem) extends Component {
    override def start: Element = 'div('class /= "display: none;")
  }
}
