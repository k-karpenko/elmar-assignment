package nl.elmar.t4s.client.app.views

import felix._
import felix.vdom.Element
import moorka._
import moorka.rx.Var
import nl.elmar.t4s.client.system.T4SaleSystem
import nl.elmar.t4s.client.system.utils.LoadingState
import nl.elmar.t4s.domain.{ShowStats, Genre, Show}
import slogging.LazyLogging

import scala.languageFeature.postfixOps
import scala.concurrent.duration._

class ShowsListView(implicit val system: T4SaleSystem) extends Component with LazyLogging {
  private val searchHandler = system.inject.showService

  private implicit val timeout = 5 seconds

  val dateFrom = Var[String]("")

  val channel = Channel[LoadingState[Seq[ShowStats]]]()

  override def start: Element = {
    'div( 'class /= "shows-view",
      'label(
        "Show date:"
      ),
      'input(
        'value =:= dateFrom
      ),
      'button( 'class /= "btn btn-primary",
        'click listen {
          dateFrom once { value ⇒
            val parts = value.split("-")
            channel pull searchHandler.search( new scalajs.js.Date(parts(0).toInt, parts(1).toInt, parts(2).toInt))
          }
        },
        "Submit"
      ),
      channel collect {
        case LoadingState.Loading ⇒ 'div("Loading...")
        case LoadingState.Error(e) ⇒ 'div("List request failed: " + e.getMessage)
        case LoadingState.Ready(p) ⇒
          val grouped = p.groupBy(_.show.genre)
          'div( 'class /= "genres",
            'h1("Results = ", grouped.size.toString ),
            DataRepeat( Buffer.fromSeq(grouped.keys.toSeq), { item: Rx[Genre] ⇒
              'div('class /= "genre",
                'h1( 'textContent := item map genreName ),
                'div( 'class /= "genre-contents",
                  item map { genre ⇒
                    'table('class /= "shows",
                      'thead(
                        'tr(
                          'th("ID"),
                          'th("Title"),
                          'th("Open Date"),
                          'th("Price"),
                          'th("Tickets Left"),
                          'th("Tickets Available")
                        )
                      ),
                      DataRepeat[ShowStats](Buffer.fromSeq(grouped(genre)), { show: Rx[ShowStats] ⇒
                        'tbody(
                          show map { r ⇒
                            'tr(
                              'td(r.show.id.value),
                              'td(r.show.title),
                              'td(new scalajs.js.Date(r.show.openDate).toString),
                              'td(r.price.toString),
                              'td(r.ticketsLeft.toString),
                              'td(r.ticketsAvailable.toString)
                            )
                          }
                        )
                      })
                    )
                  }
                )
              )
            }
            )
          )
      }
    )
  }

  private def genreName(genre: Genre): String = genre match {
    case Genre.Comedy ⇒ "Comedy"
    case Genre.Drama ⇒ "Drama"
    case Genre.Musical ⇒ "Musical"
    case _ ⇒ "Unknown Genre"
  }
}
