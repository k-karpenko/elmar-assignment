package nl.elmar.t4s.client.app.services

import moorka._
import moorka.rx.{FSM, Rx}
import nl.elmar.t4s.client.system.T4SaleSystem
import nl.elmar.t4s.client.system.utils.LoadingState
import nl.elmar.t4s.domain.{Id, Show}
import org.scalajs.dom.ext.Ajax
import org.scalajs.dom.raw.XMLHttpRequest
import upickle.default._

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.FiniteDuration
import scala.util.{Failure, Success, Try}

trait TicketService {
  def purchase(show: Id[Show], dateTimestamp: Long)
              (implicit timeout: FiniteDuration, ec: ExecutionContext, system: T4SaleSystem): Rx[Unit]
}

object TicketServiceImpl {
  private[TicketServiceImpl] val ticketUrl = "ticket"
}

class TicketServiceImpl extends TicketService {
  import TicketServiceImpl._

  override def purchase(showId: Id[Show], dateTimestamp: Long)
                       (implicit timeout: FiniteDuration, ec: ExecutionContext, system: T4SaleSystem): Rx[Unit] = {
    Ajax.post(system.inject.serverUrl + ticketUrl, write(showId → dateTimestamp), timeout.toMillis.toInt).toRx collect {
      case Success(r) ⇒ println(s"Response $r")
    }
  }
}
