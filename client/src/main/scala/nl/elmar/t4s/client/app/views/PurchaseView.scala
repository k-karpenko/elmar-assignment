package nl.elmar.t4s.client.app.views

import felix.Component
import felix.vdom.Element
import felix._
import moorka.rx.{Val, Var}
import nl.elmar.t4s.client.system.T4SaleSystem
import nl.elmar.t4s.client.system.components.SelectOptional
import nl.elmar.t4s.client.system.utils.LoadingState
import nl.elmar.t4s.domain.Show

import scala.concurrent.duration._
import scala.languageFeature.postfixOps
import scala.scalajs.js.Date

class PurchaseView(implicit val system: T4SaleSystem) extends Component {

  implicit val timeout = 5 seconds

  val showsList = Var[LoadingState[Seq[Show]]](LoadingState.Loading)
  showsList pull system.inject.showService.listAll()

  val showSelected = Var[Option[Show]](None)
  val dateSelected = Var[String]("")

  val errorMessage = Var[String]("")
  val displayErrorMessage = Var[Boolean](false)

  override def start: Element = {
    'div(
      'h1("Purchase"),
      'form(
        'div(
          'show := displayErrorMessage,
          errorMessage map( 'span(_) )
        ),
        'div( 'class /= "field",
          'label("Show"),
          new SelectOptional[Show](
            collection = showsList,
            value = showSelected,
            converter = r ⇒ r.title ,
            noneElementText = "- Select show -"
          )
        ),
        'div( 'class /= "field",
          'label("Date"),
          'input(
            'value =:= dateSelected
          )
        ),
        'button(
          'click listen {
            showSelected once { showId ⇒
              dateSelected once { date ⇒
                if ( showId.isEmpty ) {
                  errorMessage pull Val("You need to select show first")
                } else if ( date.isEmpty ) {
                  errorMessage pull Val("Date must be selected")
                } else {
                  for {
                    showIdValue ← showId
                    parts = date.split("-")
                    dateObj = new scalajs.js.Date(parts(0).toInt, parts(1).toInt, parts(2).toInt)
                  } yield {
                    system.inject.ticketService.purchase(showIdValue.id, dateObj.getTime().toLong)
                  }
                }
              }
            }
          },
          "Purchase"
        )
      )
    )
  }

}
