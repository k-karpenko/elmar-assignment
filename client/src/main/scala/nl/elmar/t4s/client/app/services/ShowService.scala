package nl.elmar.t4s.client.app.services

import java.util.Date

import felix.core.FelixSystem
import moorka.rx.{FSM, Rx}
import moorka._
import nl.elmar.t4s.client.system.T4SaleSystem
import nl.elmar.t4s.client.system.utils.LoadingState
import nl.elmar.t4s.domain._
import nl.elmar.t4s.domain.Show
import org.scalajs.dom.ext.Ajax
import org.scalajs.dom.raw.XMLHttpRequest

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.FiniteDuration
import scala.util.{Try, Failure, Success}

import upickle.default._

trait ShowService {
  def search(dateFrom: scalajs.js.Date)(implicit timeout: FiniteDuration, ec: ExecutionContext, system: T4SaleSystem):
    Rx[LoadingState[Seq[ShowStats]]]

  def listAll()(implicit timeout: FiniteDuration, ec: ExecutionContext, system: T4SaleSystem):
    Rx[LoadingState[Seq[Show]]]
}

object ShowServiceImpl {
  private def searchUrl(dateStart: scalajs.js.Date) = s"show/${dateStart.getTime()}"
  private val listAllUrl = s"list"
}

class ShowServiceImpl extends AbstractService with ShowService {
  import ShowServiceImpl._

  override def listAll()(implicit timeout: FiniteDuration,
                         ec: ExecutionContext,
                         system: T4SaleSystem): Rx[LoadingState[Seq[Show]]] = {
    FSM(LoadingState.default[Seq[Show]]) {
      case LoadingState.Loading ⇒
        val f: Rx[Try[XMLHttpRequest]] =
          Ajax.get(system.inject.serverUrl + listAllUrl, "", timeout.toMillis.toInt).toRx
        f.map {
          case Success(data) ⇒ LoadingState.Ready(read[Seq[Show]](data.responseText))
          case Failure(throwable) ⇒ LoadingState.Error(throwable)
        }
      case _ ⇒ Dummy
    }
  }

  override def search(dateFrom: scalajs.js.Date)
                     (implicit timeout: FiniteDuration,
                      ec: ExecutionContext,
                      system: T4SaleSystem): Rx[LoadingState[Seq[ShowStats]]] = {
    FSM(LoadingState.default[Seq[ShowStats]]) {
      case LoadingState.Loading ⇒
        val f: Rx[Try[XMLHttpRequest]] =
          Ajax.get(system.inject.serverUrl + searchUrl(dateFrom), "", timeout.toMillis.toInt).toRx
        f.map {
          case Success(data) ⇒ LoadingState.Ready(read[Seq[ShowStats]](data.responseText))
          case Failure(throwable) ⇒ LoadingState.Error(throwable)
        }
      case _ ⇒ Dummy
    }
  }
}